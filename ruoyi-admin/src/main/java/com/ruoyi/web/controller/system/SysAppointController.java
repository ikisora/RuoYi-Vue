package com.ruoyi.web.controller.system;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.entity.Appoint;
import com.ruoyi.common.core.domain.entity.SysDept;
import com.ruoyi.common.core.domain.entity.SysRole;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.service.*;
import org.apache.commons.lang3.ArrayUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 用户信息
 * 
 * @author ruoyi
 */
@RestController
@RequestMapping("/system/appoint")
public class SysAppointController extends BaseController
{
    @Autowired
    private ISysRoleService roleService;
    @Autowired
    private ISysAppointService iSysAppointService;
    @Autowired
    private ISysPostService postService;
    @PreAuthorize("@ss.hasPermi('system:appoint:list')")
    @GetMapping("/list")
    public TableDataInfo list(Appoint user)
    {
        startPage();
        //通过对应用户id查找对应的预约信息
        Long userId = SecurityUtils.getUserId();

        List<Appoint> list = iSysAppointService.selectAppoint(user,userId);
        return getDataTable(list);
    }


    @GetMapping(value = {  "/{id}" })
    public AjaxResult getInfo(@PathVariable(value = "id", required = false) String id)
    {

        Appoint appoint=iSysAppointService.selectById(id);
        AjaxResult ajax = AjaxResult.success();
        ajax.put(AjaxResult.DATA_TAG, appoint);
        return ajax;
    }

    @PutMapping
    public AjaxResult editAppoint(@Validated @RequestBody Appoint appoint)
    {
            return toAjax(iSysAppointService.updateAppoint(appoint));
    }
    @DeleteMapping("/{userIds}")
    public AjaxResult remove(@PathVariable Long[] userIds)
    {

        return toAjax(iSysAppointService.deleteAppointById(userIds));
    }

    @PostMapping
    public AjaxResult add(@Validated @RequestBody Appoint appoint)
    {
        Long userId = SecurityUtils.getUserId();
        appoint.setUserId(userId);
        return toAjax(iSysAppointService.addAppoint(appoint));
    }
}
