package com.ruoyi.system.service;

import com.ruoyi.common.core.domain.entity.Appoint;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 *
 * @Author: SORA
 * @Date: 2024/03/01/11:03
 * @Description:
 */
public interface ISysAppointService
{
    List<Appoint> selectAppoint(Appoint user,Long userId);

    Appoint selectById(String id);

    int updateAppoint(Appoint appoint);

    int deleteAppointById(Long[] userIds);

    int addAppoint(Appoint appoint);
}
