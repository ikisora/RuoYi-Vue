package com.ruoyi.system.service.impl;

import com.ruoyi.common.core.domain.entity.Appoint;
import com.ruoyi.common.utils.SnowFlakeUtil;
import com.ruoyi.system.domain.SysAppointDO;
import com.ruoyi.system.mapper.SysAppointMapper;
import com.ruoyi.system.mapper.SysUserMapper;
import com.ruoyi.system.service.ISysAppointService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 *
 * @Author: SORA
 * @Date: 2024/03/01/11:03
 * @Description:
 */
@Service
public class ISysAppointServiceImpl implements ISysAppointService {
    private static final Logger log = LoggerFactory.getLogger(SysUserServiceImpl.class);

    @Resource
    private SysAppointMapper sysAppointMapper;

    @Override
    public List<Appoint> selectAppoint(Appoint appoint, Long userId) {
        SysAppointDO sysAppointDO = new SysAppointDO();

        BeanUtils.copyProperties(appoint, sysAppointDO);
        List<SysAppointDO> sysAppointDOS = sysAppointMapper.selectAppointList(sysAppointDO, userId);
        List<Appoint> resultList = new ArrayList<>();
        sysAppointDOS.forEach(item -> {
            Appoint appoint1 = new Appoint();
            BeanUtils.copyProperties(item, appoint1);
            resultList.add(appoint1);
        });
        return resultList;

    }

    @Override
    public Appoint selectById(String id) {
        SysAppointDO sysAppointDO = sysAppointMapper.selectById(id);
        Appoint appoint = new Appoint();
        BeanUtils.copyProperties(sysAppointDO, appoint);

        return appoint;
    }

    @Override
    public int updateAppoint(Appoint appoint) {
        SysAppointDO sysAppointDO =new SysAppointDO();
        BeanUtils.copyProperties(appoint,sysAppointDO);

        return sysAppointMapper.updateAppoint(sysAppointDO);
    }

    @Override
    public int deleteAppointById(Long[] userIds) {
        List<Long> longs = Arrays.asList(userIds);
       return sysAppointMapper.deleteByIdList(longs);

    }

    @Override
    public int addAppoint(Appoint appoint) {
        String snowflake = SnowFlakeUtil.getSnowFlakeId().toString();
        appoint.setId(snowflake);
        return sysAppointMapper.addAppoint(appoint);
    }

}
