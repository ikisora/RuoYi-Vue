package com.ruoyi.system.mapper;

import com.ruoyi.common.core.domain.entity.Appoint;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.system.domain.SysAppointDO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * 预约表 数据层
 * 
 * @author ruoyi
 */
@Mapper
public interface SysAppointMapper
{

    public List<SysAppointDO> selectAppointList(SysAppointDO appoint,@Param("userId") Long userId);


    SysAppointDO selectById( String id);

    int updateAppoint(SysAppointDO sysAppointDO);

    int deleteByIdList(@Param("longs") List<Long> longs);

    int addAppoint(Appoint appoint);
}
