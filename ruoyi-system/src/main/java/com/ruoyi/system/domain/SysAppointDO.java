package com.ruoyi.system.domain;

import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 *
 * @Author: SORA
 * @Date: 2024/03/01/10:57
 * @Description:
 */
public class SysAppointDO {
private String id ;
private String customerName;
private String phoneNumber;
private Date date;
private String remark;
private String status;
private String steelPlateType;
private Date createdTime;
private Long userId;


    public SysAppointDO() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSteelPlateType() {
        return steelPlateType;
    }

    public void setSteelPlateType(String steelPlateType) {
        this.steelPlateType = steelPlateType;
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }
}
